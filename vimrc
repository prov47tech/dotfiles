set nocompatible
syntax on
set showmode
set showcmd
set number
set encoding=utf-8
color dracula
set termguicolors
filetype plugin on
