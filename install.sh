#!/bin/bash                                                                                                                                                                                                    
#                                                                                                                                                                                                  
#Establish Dotfiles Directory                                                                                                                                                                                  
#                                                                                                                                                                                                              
#Create Directories
mkdir -p ~/projects/go/{bin,pkg,src}
mkdir .ssh
#
#Clone Repositories
git clone https://github.com/dracula/vim ~/.vim/pack/plugins/start/dracula
git clone https://gitlab.com/prov47tech/dotfiles ~/projects/dotfiles
#Symlink the files                                                                                                                                                                                             
#                                                                                                                                                                                                              
ln -sfv /root/projects/dotfiles/gitconfig ~/.gitconfig
ln -sfv /root/projects/dotfiles/gitignore ~/.gitignore
ln -sfv /root/projects/dotfiles/vimrc ~/.vimrc
ln -sfv /root/projects/dotfiles/config ~/.ssh/config
ln -sfv /root/projects/dotfiles/tmux.conf ~/.tmux.conf
ln -sfv /root/projects/dotfiles/bashrc ~/.bashrc

